class InvalidSender(Exception):
    def __init__(self, type):
        self.message = f"Invalid sender type provided: {type}"
        super().__init__(self.message)

class InvalidAttributeCount(Exception):
    def __init__(self, got_count, expected_count):
        self.message = f"Invalid attribute count, got: {got_count}, expected: {expected_count}"

class InvalidMessageTemplate(Exception):
    def __init__(self, message):
        self.message = message
