class Template:
    def __init__(self, type:str, content:str):
        self.type = type 
        self.content = content 
    
    def get_type(self):
        return self.type 
    def get_content(self):
        return self.content 
    
    def get_attribute_count(self):
        attribute_count = self.content.count("{}")
        return attribute_count

