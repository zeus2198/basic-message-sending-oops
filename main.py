from message.message import Message
from sender.sender_factory import get_sender
from template.template import Template
from user.user import User

john = User(name="John", email="john@acme.inc", phone=1234567819)

welcome_template = Template(type="text", content="Hey {}, welcome to {}!")
welcome_message = Message(
    template=welcome_template,
    attributes=["John", "LimeChat"]
)

my_sender = get_sender("email")
print("\n")
my_sender.send(john, welcome_message)

my_sender = get_sender("sms")
print("\n")
my_sender.send(john, welcome_message)

