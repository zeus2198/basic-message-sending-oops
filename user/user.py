class User():
    def __init__(self, name:str, email:str, phone:int):
        self.name = name 
        self.email = email 
        self.phone = phone

    def get_name(self) -> str:
        return self.name 
    
    def get_email(self) -> str:
        return self.email 
    
    def get_phone(self) -> int:
        return self.phone
