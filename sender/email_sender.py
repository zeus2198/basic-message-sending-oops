from .sender import Sender


class EmailSender(Sender):
    
    def send(self, user, message) -> bool:
        # SMS Sending logic goes here
        # ideally we will be calling an API to send our message
        email = user.get_email()
        message_text = message.build_message()
        print(f"Sending Email: message: {message_text} to {email}")
        return True 
    