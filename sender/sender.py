from abc import ABC, abstractmethod

from message.message import Message
from user.user import User


class Sender(ABC):       
    @abstractmethod
    def send(self, user:User, message:Message) -> bool:
        pass
