from exceptions import InvalidMessageTemplate

from .sender import Sender


class SmsSender(Sender):
    
    def send(self, user, message) -> bool:
        # SMS Sending logic goes here
        # ideally we will be calling an API to send our message
        phone = user.get_phone()
        template_type = message.get_template_type()
        if template_type != "text":
            raise InvalidMessageTemplate("SMS sender can only send message template of text types")
        message_text = message.build_message()
        print(f"Sending SMS: message: {message_text} to {phone}")
        return True 
    