from exceptions import InvalidSender

from .email_sender import EmailSender
from .sender import Sender
from .sms_sender import SmsSender


def get_sender_bad(sender_type:str) -> Sender:
    if sender_type == "email":
        return EmailSender()
    elif sender_type == "sms":
        return SmsSender()
    else:
        raise InvalidSender(sender_type)

SENDERS = {
    "email": EmailSender,
    "sms": SmsSender
}

def get_sender(sender_type:str) -> Sender:
    if sender_type not in SENDERS:
        raise InvalidSender(sender_type)
    return SENDERS[sender_type]()
