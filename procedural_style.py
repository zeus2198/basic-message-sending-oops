# you are getting this data via api / db:
user_email = "john@acme.inc"
user_phone = 1234567819
template_content = "Hey {}, welcome to {}!"
template_type = "text"

sender = "sms"
attributes = ["John", "LimeChat"]

attribute_count_in_template = template_content.count("{}")
if attribute_count_in_template != len(attributes):
    raise Exception("Invalid Attribute count")


message = template_content 
for attribute in attributes:
    message = message.replace("{}", attribute, 1)

if sender == 'sms' and template_type != 'text':
    raise Exception("SMS sender can only send templates of type text")

if sender == 'email': 
    print(f"Sending Email: message: {message} to {user_email}")
elif sender == 'sms':
    print(f"Sending SMS: message: {message} to {user_phone}")
else:
    raise Exception("Invalid sender")
