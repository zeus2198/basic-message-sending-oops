from exceptions import InvalidAttributeCount
from template.template import Template


class Message():
    def __init__(self, template:Template, attributes:list):
        self.template = template
        self.attributes = attributes 
        attribute_count_in_template = self.template.get_attribute_count()
        if attribute_count_in_template != len(self.attributes):
            raise InvalidAttributeCount(got_count=len(self.attributes), expected_count=attribute_count_in_template)

    def get_template_type(self) -> str:
        return self.template.get_type()
    
    def build_message(self) -> str:
        content = self.template.get_content()
        for attribute in self.attributes:
            content = content.replace("{}", attribute, 1)
        return content 
        
